package practice.practice20190402;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Practice20190402Application {

	public static void main(String[] args) {
		SpringApplication.run(Practice20190402Application.class, args);
	}

}
